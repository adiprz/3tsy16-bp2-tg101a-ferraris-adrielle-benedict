#include "Spell.h"
#include "Wizard.h"


Spell::Spell(string name, int damage, int mpCost)
{
	mName = name;
	mDamage = damage;
	mMpCost = mpCost;
}


Spell::~Spell()
{
}

string Spell::getName()
{
	return mName;
}

int Spell::getDamage()
{
	return mDamage;
}

int Spell::getMpCost()
{
	return mMpCost;
}

void Spell::activate(Wizard * actor, Wizard * target)
{
	if (mName == "Lifesteal")
	{
		if (actor->getMp() >= mMpCost) {
			cout << actor->getName() << " casts " << mName << " on " << target->getName() << endl;
			actor->reduceMp(mMpCost);
			target->takeDamage(mDamage);
			actor->setHp(actor->getHp() + mDamage);
		}
	}
	else {
		if (actor->getMp() >= mMpCost) {
			cout << actor->getName() << " casts " << mName << " on " << target->getName() << endl;
			actor->reduceMp(mMpCost);
			target->takeDamage(mDamage);
		}
		else {
			cout << "You don't have enough mp" << endl;
		}
	}
}
}
