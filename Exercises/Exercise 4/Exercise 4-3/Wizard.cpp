#include "Wizard.h"



Wizard::Wizard(string name, int mp, int hp)
{
	mName = name;
	mMp = mp;
	mMpMax = mp;
	mHp = hp;
	mHpMax = hp;
	//mSpell = new Spell(*spell);//Copy syntax
	mSpell = new Spell("Earth Triangle", 3, 2);

	Spell* s1 = new Spell("Fireball", 5, 2);
	Spell* s2 = new Spell("Burning Ice", 10, 1);
	Spell* s3 = new Spell("Wet Noodle", 1, 1);
	mSpells.push_back(s1);
	mSpells.push_back(s2);
	mSpells.push_back(s3);
}

Wizard::~Wizard()
{
}

int Wizard::getMp()
{
	return mMp;
}

string Wizard::getName()
{
	return mName;
}

int Wizard::getHp()
{
	return mHp;
}

void Wizard::setHp(int hp)
{
	mHp = hp;
	if (mHp > mHpMax) // Clamp to max
		mHp = mHpMax;
}

void Wizard::reduceMp(int mp)
{
	mMp -= mp;
	if (mMp < 0)
		mMp = 0;
}

void Wizard::takeDamage(int damage)
{
	mHp -= damage;
	// Clamp min to 0
	if (mHp < 0)
		mHp = 0;
}

void Wizard::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << "/" << mHpMax << endl;
	cout << "MP: " << mMp << "/" << mMpMax << endl;
}

void Wizard::castSpell(Wizard * target)
{
	mSpell->activate(this, target);
}
