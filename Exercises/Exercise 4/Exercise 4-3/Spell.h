#pragma once
#include <string>
using namespace std;

class Wizard;
class Spell
{
public:
	Spell(string name, int damage, int mpCost);
	~Spell();

	string getName();
	int getDamage();
	int getMpCost();

	void activate(Wizard *actor, Wizard *target);
private:
	string mName;
	int mDamage;
	int mMpCost;
};

