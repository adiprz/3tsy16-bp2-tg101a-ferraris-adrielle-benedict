#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Spell.h"
using namespace std;

class Wizard
{
public:
	Wizard(string name, int mp, int hp);
	~Wizard();

	int getMp();
	string getName();
	int getHp();
	void setHp(int hp);
	void reduceMp(int mp);
	void takeDamage(int damage);
	void printStats();
	void castSpell(Wizard* target);
private:
	string mName;
	int mMp;
	int mMpMax;
	int mHp;
	int mHpMax;
	Spell* mSpell;
	vector<Spell*> mSpells;
};

