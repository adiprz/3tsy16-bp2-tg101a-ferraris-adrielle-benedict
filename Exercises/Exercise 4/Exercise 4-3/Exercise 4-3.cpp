#include <iostream>
#include <string>
#include "Wizard.h"
using namespace std;

int main()
{
	// Input name
	string name;
	cout << "Input name: ";
	cin >> name;

	// Input HP
	int hp;
	cout << "Input HP: ";
	cin >> hp;

	// Input MP
	int mp;
	cout << "Input MP: ";
	cin >> mp;

	Wizard* player = new Wizard(name, mp, hp);
	Wizard* enemy = new Wizard("Shitcode", 100, 100);

	player->printStats();
	enemy->printStats();

	while (player->getHp() > 0 && enemy->getHp() > 0) {
		player->castSpell(enemy);
		enemy->castSpell(player);

		player->printStats();
		enemy->printStats();
	}


	system("pause");
	return 0;
}