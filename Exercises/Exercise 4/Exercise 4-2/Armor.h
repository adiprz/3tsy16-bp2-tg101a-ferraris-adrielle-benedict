#pragma once
#include "Materia.h"
#include <vector>
#include <string>

class Armor
{
public:
	Armor(std::string name, int def);
	~Armor();

	std::string getName();
	int getDef();

	void insertMateria(Materia* materia);
	Materia* removeMateria(int index);
	void clearAllMateria();

private:
	std::string mName;
	int mDef;
	std::vector<Materia*> mMaterias;
};

