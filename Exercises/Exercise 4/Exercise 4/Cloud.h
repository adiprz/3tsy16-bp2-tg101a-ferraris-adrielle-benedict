#ifndef CLOUD_H
#define CLOUD_H
#include <string>

using namespace std;

class Cloud
{
public:
	void XCut();
	void Magic();
	void DBlow();
	void WItem();
private:
	string name;
	int level;
	int hitPoints;
	int magicPoints;
	int str;
	int dex;
	int vit;
	int magic;
	int spirit;
	int luck;
	int attack;
	int attackPercentage;
	int defense;
	int defensePercentage;
	int magicAttack;
	int magicDef;
	int magicDefPercentage;
	int exp;
	int expTillNextLevel;
	int limitLevel;
};
#endif