#include "Circle.h"




Circle::Circle(float radius)
{
	mRadius = radius;
}

Circle::~Circle()
{
}

string Circle::type()
{
	return "Circle";
}

float Circle::perimeter()
{
	return 2 * 3.14f * mRadius;
}

float Circle::area()
{
	return 3.14f * (mRadius * mRadius);
}
