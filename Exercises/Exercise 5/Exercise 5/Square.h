#pragma once
#include "Shape.h"
class Square :
	public Shape
{
public:
	Square(float length);
	~Square();

	string type();
	float perimeter();
	float area();

private:
	float mLength;
};

