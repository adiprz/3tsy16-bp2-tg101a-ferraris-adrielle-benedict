#pragma once
#include <string>

using namespace std;

class Shape
{
public:
	Shape();
	~Shape();

	virtual string type() = 0; //Pure virtual function. This doesn't have an implementation
	virtual float perimeter() = 0;
	virtual float area() = 0;
};

