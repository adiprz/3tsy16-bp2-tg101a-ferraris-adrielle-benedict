#pragma once
#include "Shape.h"
class Circle :
	public Shape
{
public:
	Circle(float radius);
	~Circle();

	string type();
	float perimeter();
	float area();

private:
	float mRadius;
};

