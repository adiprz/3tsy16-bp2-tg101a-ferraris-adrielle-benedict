#include "StatBoostSkill.h"


// Invoke the constructor of the parent class. This is required if you are not using parameterless constructor for the parent.
StatBoostSkill::StatBoostSkill(string name, StatType type, int bonus)
	: Skill(name)
{
	mType = type;
	mBonus = bonus;
}

StatBoostSkill::~StatBoostSkill()
{
}

void StatBoostSkill::activate(Unit * target)
{
	// Append the implementation of the parent class by manually calling its function
	Skill::activate(target);

	string statName;

	// Add stat to the set type
	switch (mType)
	{
	case StatPower:
		statName = "Power";
		target->getStats().power += mBonus;
		break;
	case StatVitality:
		statName = "Vitality";
		target->getStats().vitality += mBonus;
		break;
	case StatDexterity:
		statName = "Dexterity";
		target->getStats().dexterity += mBonus;
		break;
	case StatAgility:
		statName = "Agility";
		target->getStats().agility += mBonus;
		break;
	default:
		break;
	}

	cout << target->getName() << "'s " << statName << " is increased by " << mBonus << endl;
}
