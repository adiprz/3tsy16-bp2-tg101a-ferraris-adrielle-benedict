#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include "Unit.h"
#include "StatBoostSkill.h"
#include "HealSkill.h"

using namespace std;

Unit* createUnit();

int main()
{
	srand(time(0));

	Unit* unit = createUnit();

	while (true)
	{
		// Display stats
		unit->displayStatus();
		cout << endl;

		// Get random skill
		int skillIndex = rand() % unit->getSkills().size();
		Skill* skill = unit->getSkills()[skillIndex];

		// Cast to self
		skill->activate(unit);

		system("pause");
		system("cls");
	}

	delete unit;

	system("pause");
	return 0;
}

Unit * createUnit()
{
	// Initialize stats
	Stats stats;
	stats.hp = 100;
	stats.power = 5;
	stats.vitality = 5;
	stats.agility = 5;
	stats.dexterity = 5;

	// Create the unit
	Unit* unit = new Unit("Hero", stats);

	// Create skills and add it to the unit
	Skill* heal = new HealSkill("Heal", 10); // Doesn't matter if it's of type Skill or HealingSkill. In the end, it's identity will be just a Skill (from Unit::getSkills()
	unit->addSkill(heal);

	Skill* might = new StatBoostSkill("Might", StatPower, 2);
	unit->addSkill(might);

	Skill* ironSkin = new StatBoostSkill("Iron Skin", StatVitality, 2);
	unit->addSkill(ironSkin);

	Skill* concentration = new StatBoostSkill("Concentration", StatDexterity, 2);
	unit->addSkill(concentration);

	Skill* haste = new StatBoostSkill("Haste", StatAgility, 2);
	unit->addSkill(haste);

	return unit;
}
