#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

// Struct
struct Roll
{
	string type;
	int rank;
	int value;
	int dice[3];
};

// Functions
void wager(int &money, int &bet)
{
	bool state = true;

	while (state)
	{
		if (bet < 100 || bet > money)
		{
			system("cls");
			cout << "Current Perica: " << money << endl;
			cout << "Enter bet (minimum of 100 Perica): ";
			cin >> bet;
		}
		else
		{
			cout << "You have entered: " << bet << " Perica." << endl << endl;
			money -= bet;
			state = false;
		}
	}
}

int payout(int &money, int result, int bet)
{
	if (result == 1)
	{
		cout << "You won!" << endl;
		bet *= 2;
		money += bet;
	}
	else
	{
		cout << "You lose! :(" << endl;
		money -= bet;
	}
	return money;
}

Roll diceRollPlayer()
{
	srand(time(0));

	Roll diceRollPlayer;
	int pisser = rand() % 20 + 1;
	diceRollPlayer.dice[0] = rand() % 6 + 1;
	diceRollPlayer.dice[1] = rand() % 6 + 1;
	diceRollPlayer.dice[2] = rand() % 6 + 1;

	// Check for Pisser
	if (pisser == 1)
	{
		diceRollPlayer.type = "Pisser";
		diceRollPlayer.rank = 1;
		diceRollPlayer.value = 1;
		return diceRollPlayer;
	}

	// Check for Snake Eyes
	else if (diceRollPlayer.dice[0] == 1 && diceRollPlayer.dice[1] == 1 && diceRollPlayer.dice[2] == 1)
	{
		diceRollPlayer.type = "Snake Eyes";
		diceRollPlayer.rank = 6;
		diceRollPlayer.value = 6;
		return diceRollPlayer;
	}

	// Check for Triples
	else if (diceRollPlayer.dice[0] == diceRollPlayer.dice[1] == diceRollPlayer.dice[2])
	{
		diceRollPlayer.type = "Triples";
		diceRollPlayer.rank = 5;
		diceRollPlayer.value = 5;
		return diceRollPlayer;
	}

	// Check for 4-5-6
	else if ((diceRollPlayer.dice[0] == 4 && diceRollPlayer.dice[1] == 5 && diceRollPlayer.dice[2] == 6) ||
		(diceRollPlayer.dice[0] == 4 && diceRollPlayer.dice[1] == 6 && diceRollPlayer.dice[2] == 5) ||
		(diceRollPlayer.dice[0] == 5 && diceRollPlayer.dice[1] == 4 && diceRollPlayer.dice[2] == 6) ||
		(diceRollPlayer.dice[0] == 5 && diceRollPlayer.dice[1] == 6 && diceRollPlayer.dice[2] == 4) ||
		(diceRollPlayer.dice[0] == 6 && diceRollPlayer.dice[1] == 4 && diceRollPlayer.dice[2] == 5) ||
		(diceRollPlayer.dice[0] == 6 && diceRollPlayer.dice[1] == 5 && diceRollPlayer.dice[2] == 4))
	{
		diceRollPlayer.type = "4-5-6";
		diceRollPlayer.rank = 4;
		diceRollPlayer.value = 4;
		return diceRollPlayer;
	}

	// Check for Pair
	else if (diceRollPlayer.dice[0] == diceRollPlayer.dice[1])
	{
		diceRollPlayer.type = "Pair";
		diceRollPlayer.rank = 3;
		diceRollPlayer.value = diceRollPlayer.dice[2];
		return diceRollPlayer;
	}
	else if (diceRollPlayer.dice[1] == diceRollPlayer.dice[2])
	{
		diceRollPlayer.type = "Pair";
		diceRollPlayer.rank = 3;
		diceRollPlayer.value = diceRollPlayer.dice[0];
		return diceRollPlayer;
	}
	else if (diceRollPlayer.dice[0] == diceRollPlayer.dice[2])
	{
		diceRollPlayer.type = "Pair";
		diceRollPlayer.rank = 3;
		diceRollPlayer.value = diceRollPlayer.dice[1];
		return diceRollPlayer;
	}

	// Check for 1-2-3
	else if ((diceRollPlayer.dice[0] == 1 && diceRollPlayer.dice[1] == 2 && diceRollPlayer.dice[2] == 3) ||
		(diceRollPlayer.dice[0] == 1 && diceRollPlayer.dice[1] == 3 && diceRollPlayer.dice[2] == 2) ||
		(diceRollPlayer.dice[0] == 2 && diceRollPlayer.dice[1] == 1 && diceRollPlayer.dice[2] == 3) ||
		(diceRollPlayer.dice[0] == 2 && diceRollPlayer.dice[1] == 3 && diceRollPlayer.dice[2] == 1) ||
		(diceRollPlayer.dice[0] == 3 && diceRollPlayer.dice[1] == 1 && diceRollPlayer.dice[2] == 2) ||
		(diceRollPlayer.dice[0] == 3 && diceRollPlayer.dice[1] == 2 && diceRollPlayer.dice[2] == 1))
	{
		diceRollPlayer.type = "1-2-3";
		diceRollPlayer.rank = 2;
		diceRollPlayer.value = 2;
		return diceRollPlayer;
	}

	// Check for Bust
	else
	{
		diceRollPlayer.type = "Bust";
		diceRollPlayer.rank = 0;
		diceRollPlayer.value = 0;
		return diceRollPlayer;
	}
}

Roll diceRollDealer()
{
	srand(time(0));

	Roll diceRollDealer;
	int pisser = rand() % 20 + 1;
	diceRollDealer.dice[0] = rand() % 6 + 1;
	diceRollDealer.dice[1] = rand() % 6 + 1;
	diceRollDealer.dice[2] = rand() % 6 + 1;

	// Check for Pisser
	if (pisser == 1)
	{
		diceRollDealer.type = "Pisser";
		diceRollDealer.rank = 1;
		diceRollDealer.value = 1;
		return diceRollDealer;
	}

	// Check for Snake Eyes
	else if (diceRollDealer.dice[0] == 1 && diceRollDealer.dice[1] == 1 && diceRollDealer.dice[2] == 1)
	{
		diceRollDealer.type = "Snake Eyes";
		diceRollDealer.rank = 6;
		diceRollDealer.value = 6;
		return diceRollDealer;
	}

	// Check for Triples
	else if (diceRollDealer.dice[0] == diceRollDealer.dice[1] == diceRollDealer.dice[2])
	{
		diceRollDealer.type = "Triples";
		diceRollDealer.rank = 5;
		diceRollDealer.value = 5;
		return diceRollDealer;
	}

	// Check for 4-5-6
	else if ((diceRollDealer.dice[0] == 4 && diceRollDealer.dice[1] == 5 && diceRollDealer.dice[2] == 6) ||
		(diceRollDealer.dice[0] == 4 && diceRollDealer.dice[1] == 6 && diceRollDealer.dice[2] == 5) ||
		(diceRollDealer.dice[0] == 5 && diceRollDealer.dice[1] == 4 && diceRollDealer.dice[2] == 6) ||
		(diceRollDealer.dice[0] == 5 && diceRollDealer.dice[1] == 6 && diceRollDealer.dice[2] == 4) ||
		(diceRollDealer.dice[0] == 6 && diceRollDealer.dice[1] == 4 && diceRollDealer.dice[2] == 5) ||
		(diceRollDealer.dice[0] == 6 && diceRollDealer.dice[1] == 5 && diceRollDealer.dice[2] == 4))
	{
		diceRollDealer.type = "4-5-6";
		diceRollDealer.rank = 4;
		diceRollDealer.value = 4;
		return diceRollDealer;
	}

	// Check for Pair
	else if (diceRollDealer.dice[0] == diceRollDealer.dice[1])
	{
		diceRollDealer.type = "Pair";
		diceRollDealer.rank = 3;
		diceRollDealer.value = diceRollDealer.dice[2];
		return diceRollDealer;
	}
	else if (diceRollDealer.dice[1] == diceRollDealer.dice[2])
	{
		diceRollDealer.type = "Pair";
		diceRollDealer.rank = 3;
		diceRollDealer.value = diceRollDealer.dice[0];
		return diceRollDealer;
	}
	else if (diceRollDealer.dice[0] == diceRollDealer.dice[2])
	{
		diceRollDealer.type = "Pair";
		diceRollDealer.rank = 3;
		diceRollDealer.value = diceRollDealer.dice[1];
		return diceRollDealer;
	}

	// Check for 1-2-3
	else if ((diceRollDealer.dice[0] == 1 && diceRollDealer.dice[1] == 2 && diceRollDealer.dice[2] == 3) ||
		(diceRollDealer.dice[0] == 1 && diceRollDealer.dice[1] == 3 && diceRollDealer.dice[2] == 2) ||
		(diceRollDealer.dice[0] == 2 && diceRollDealer.dice[1] == 1 && diceRollDealer.dice[2] == 3) ||
		(diceRollDealer.dice[0] == 2 && diceRollDealer.dice[1] == 3 && diceRollDealer.dice[2] == 1) ||
		(diceRollDealer.dice[0] == 3 && diceRollDealer.dice[1] == 1 && diceRollDealer.dice[2] == 2) ||
		(diceRollDealer.dice[0] == 3 && diceRollDealer.dice[1] == 2 && diceRollDealer.dice[2] == 1))
	{
		diceRollDealer.type = "1-2-3";
		diceRollDealer.rank = 2;
		diceRollDealer.value = 2;
		return diceRollDealer;
	}

	// Check for Bust
	else
	{
		diceRollDealer.type = "Bust";
		diceRollDealer.rank = 0;
		diceRollDealer.value = 0;
		return diceRollDealer;
	}
}

void playRound(int &round, int &money)
{
	int bet = 0;

	// Game Proper
	cout << "Round: " << round << endl;
	system("pause");
	system("cls");
	wager(money, bet);
	int result = rand() % 2;
	payout(money, result, bet);
	cout << "Remaning Perica: " << money << endl;
	system("pause");
	system("cls");
}

int main()
{
	int perica = 90000;
	
	for (int counter = 1; counter < 11; counter++)
	{
		playRound(counter, perica);
	}

	system("pause");
	return 0;
}