#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

// Struct
struct Roll
{
	int dice[3];
};

Roll* diceRoll()
{
	srand(time(0));

	Roll* dynRoll = new Roll;
	dynRoll->dice[0] = rand() % 6 + 1;
	dynRoll->dice[1] = rand() % 6 + 1;
	dynRoll->dice[2] = rand() % 6 + 1;

	cout << dynRoll->dice[0] << endl;
	cout << dynRoll->dice[1] << endl;
	cout << dynRoll->dice[2] << endl;

	return dynRoll;
}

int main()
{
	diceRoll();

	system("pause");
	return 0;
}