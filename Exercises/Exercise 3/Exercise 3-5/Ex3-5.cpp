#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

// Struct
struct Roll
{
	int dice[3];
	int rank;
};

Roll* diceRoll()
{
	srand(time(0));

	Roll* dynRoll = new Roll;
	dynRoll->dice[0] = rand() % 6 + 1;
	dynRoll->dice[1] = rand() % 6 + 1;
	dynRoll->dice[2] = rand() % 6 + 1;

	cout << dynRoll->dice[0] << endl;
	cout << dynRoll->dice[1] << endl;
	cout << dynRoll->dice[2] << endl;

	dynRoll->rank = rand() % 10 + 1;

	return dynRoll;
}

Roll* rollUntilValid()
{
	for (int counter = 0; counter < 3; counter++)
	{
		Roll* roll = diceRoll();
		if (roll->rank != 0) {
			return roll;
		}
		
		delete roll;
	}
}

void playRound(int &money)
{
	int bet = 0;

	// Rolls for player and dealer
	Roll* rollingUntilValid = rollUntilValid();

	// Payout

	// Delete
}

void wager(int &money, int bet)
{

}

void payout(Roll* playerRoll, Roll* dealerRoll, int &money, int bet)
{

}

int main()
{
	int money = 90000;
	for (int i = 0; i < 10; i++)
	{
		playRound(money);
	}

	system("pause");
	return 0;
}