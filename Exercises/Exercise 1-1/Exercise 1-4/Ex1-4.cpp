#include <iostream>

using namespace std;

int findItem(string items[3], string searchKey)
{
	for (int counter = 0; counter < 3; counter++)
	{
		if (items[counter] == searchKey)
		{
			return counter;
		}
	}

	return -1;
}

int main()
{
	string items[3] = { "Red Potion", "Panacea", "Short Sword" };

	system("pause");
	return 0;
}