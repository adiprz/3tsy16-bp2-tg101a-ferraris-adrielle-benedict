#include <iostream>

using namespace std;

bool findItem(string items[3], string searchKey)
{
	for (int counter = 0; counter < 3; counter++)
	{
		if (items[counter] == searchKey)
		{
			return true;
		}
	}

	return false;
}

int main()
{
	string items[3] = { "Red Potion", "Panacea", "Short Sword" };

	system("pause");
	return 0;
}