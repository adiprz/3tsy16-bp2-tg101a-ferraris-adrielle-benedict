#include <iostream>
#include <string>

using namespace std;

void printFunction(int num[5])
{
	for (int counter = 0; counter < 5; counter++)
	{
		cout << num[counter] << endl;
	}
}

int main()
{
	int num[5] = { 1, 2, 3, 4, 5 };
	printFunction(num);
	system("pause");
	return 0;
}