#include <iostream>
#include <string>

using namespace std;

// Function
int factorial(int fact)
{
	if (fact == 0)
	{
		return 1;
	}

	int result = 1;
	for (int counter = 1; counter <= fact; counter++)
	{
		result *= counter;
	}

	return result;
}

int main()
{
	int num;
	cout << "Please enter the number you would like to factor: ";
	cin >> num;

	int result = factorial(num);
	cout << "Factorial of " << num << "! is = " << result << endl << endl;

	system("pause");
	return 0;
}