#include <iostream>
#include <string>

using namespace std;

// Function for suggesting a package
void suggestAPackage(int packages[], int startGold, int itemPrice)
{
	if (startGold >= itemPrice)
	{
		// Can buy
	}
	else
	{
		int goldNeeded = itemPrice - startGold;
		for (int counter = 0; counter < 7; counter++)
		{
			if (packages[counter] >= goldNeeded)
			{
				// wtf
			}
		}
	}
}
// Function for Unsorted to Sorted IAP Package Listing
void sortedPackages(int packages[])
{
	int tempNumber;
	for (int counter = 0; counter < 7 - 1; counter++)
	{
		for (int counter2 = counter; counter2 < 7; counter2++)
		{
			if (packages[counter] > packages[counter2])
			{
				tempNumber = packages[counter];
				packages[counter] = packages[counter2];
				packages[counter2] = tempNumber;
			}
		}
	}
}

int main()
{
	// Variables
	int itemPrice = 0;
	int startGold = 250;
	string answer1 = "";
	string answer2 = "";

	// Output	
	while (startGold > 0)
	{
		// IAP Package Listing
		int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
		sortedPackages(packages);

		for (int x = 0; x < 7; x++)
		{
			cout << "Package #" << x << ": " << packages[x] << " Gold" << endl;
		}

		// Output
		cout << endl;
		cout << "Gold: " << startGold << endl;
		cout << "Please enter the price of the item you would like to purchase: ";
		cin >> itemPrice;

		if (itemPrice < startGold)
		{
			startGold -= itemPrice;
			cout << "Gold: " << startGold << endl;
		}
		else if (itemPrice > startGold)
		{
			cout << "You do not have enough Gold to purchase the item." << endl;
			system("pause");
			return 0;
		}

		// Asks user if he/she will keep shopping
		cout << "Would you like to keep shopping? (Y)es / (N)o : ";
		cin >> answer2;

		if (answer2 == "y" || answer2 == "Y")
		{
			continue;
		}

		if (answer2 == "n" || answer2 == "N")
		{
			return 0;
		}
	}
}