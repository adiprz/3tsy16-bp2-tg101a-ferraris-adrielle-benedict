#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Node
{
	string name;
	Node* next = NULL;
	Node* structPrevious = NULL;
};

Node * circularLinkedList(int soldierCount);
Node * soldierNames(Node * head, int soldierCount);
void printRemainingSoldiers(Node * head, int soldierCount, int rounds);
Node * cloakPass(Node * head, int randomValue);
Node * removeSoldier(Node * head, int randomValue);

int main() {

	// Console Design
	cout << "====================================================================" << endl;
	cout << "                        T H E  W A L L" << endl;
	cout << "====================================================================" << endl << endl;

	// Variables
	int soldierCount;
	int rounds = 1;

	// Number of soldiers to be deployed
	cout << "How many soldiers will you deploy, Lord Commander? ";
	cin >> soldierCount;

	// Assigns soldierCount to currentSoldiers for data storage
	int currentSoldiers = soldierCount;

	// Creates the circular linked list
	Node* head = circularLinkedList(currentSoldiers);

	// Input for soldier names
	soldierNames(head, currentSoldiers);
	system("cls");

	// RNG for the starting point
	srand(time(0));
	int startingPoint = rand() % currentSoldiers + 1;

	// Randomly selects the starting point
	head = cloakPass(head, startingPoint);

	// Loops until one soldier is remaining
	while (currentSoldiers > 1)
	{

		// Prints the current/remaining soldiers
		printRemainingSoldiers(head, currentSoldiers, rounds);

		// Seeds the random number for the passing of the cloak
		int selector = rand() % currentSoldiers + 1;

		// Shows the result of the randomly generated draw lot
		cout << "Result: " << endl << " " << head->name << " has drawn " << selector << endl;

		// Outputs the soldier who has received the cloak
		Node* cloak = cloakPass(head, selector);
		cout << " " << cloak->name << " has been eliminated " << endl << endl;

		// Removes the soldier who received the cloak
		head = removeSoldier(head, selector);

		// Lessens the soldierCount by 1 and increments a round
		currentSoldiers--;
		rounds++;

		system("pause");
		system("cls");
	}

	// Displays Final Result
	cout << "====================================================================" << endl;
	cout << "                       F I N A L  R E S U L T" << endl;
	cout << "====================================================================" << endl << endl;
	cout << "            " << head->name << " will depart to seek for reinforcements." << endl << endl;

	system("pause");
	return 0;
}

Node * circularLinkedList(int soldierCount)
{
	Node* head = NULL;
	Node* current = NULL;
	Node* previous = NULL;

	// Creates a new node to store the pointers
	current = new Node;
	previous = current;
	head = current;

	// Loops until the counter is not less than the number of soldiers
	for (int counter = 0; counter < soldierCount - 1; counter++)
	{
		// Creates a new node to store the data of the inputted soldiers
		current = new Node;
		current->structPrevious = previous;
		previous->next = current;
		previous = current;
	}

	// Connects each node to one another to achieve a circular linked list
	current->next = head;
	head->structPrevious = current;

	return head;
}

Node * soldierNames(Node * head, int soldierCount)
{
	cout << endl;

	// Loops until you reach your desired amount of soldiers
	for (int counter = 0; counter < soldierCount; counter++)
	{
		cout << " " << "State your name, soldier. ";
		// Stores the name of each soldier
		cin >> head->name;
		// Switches the current pointer to the next node
		head = head->next;
	}

	cout << endl;
	return head;
}

void printRemainingSoldiers(Node * head, int soldierCount, int rounds)
{
	// Round Banner
	cout << "==========================================" << endl;
	cout << "               R O U N D  " << rounds << endl;
	cout << "==========================================" << endl << endl;
	cout << "Remaining Soldiers:" << endl;

	// Displays your soldiers
	for (int counter = 0; counter < soldierCount; counter++)
	{
		cout << " " << head->name << endl;
		head = head->next;
	}

	cout << endl;
}

Node * cloakPass(Node * head, int randomValue)
{
	for (int counter = 0; counter < randomValue; counter++)
		head = head->next;

	return head;
}

Node* removeSoldier(Node * head, int randomValue)
{
	// Store previous node so we can reconnect it to the tail of the deleted node.
	Node* previous = NULL;
	Node* deleteMember = head;

	// Loops until we get to the specific node that we want to delete
	for (int counter = 0; counter < randomValue; counter++)
	{
		previous = deleteMember;
		deleteMember = deleteMember->next;
	}

	// Set the "next" pointer of the previous node to the "next" pointer of the toDelete node.
	previous->next = deleteMember->next;
	head = deleteMember->next;
	// Deletes deleteMember node from our memory pool so we can reclaim the used memory
	delete deleteMember;

	return head;
}