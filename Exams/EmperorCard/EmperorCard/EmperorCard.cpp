#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <vector>

using namespace std;

// Function Declarations
void consoleOutput(int perica, int drillDistance, int round, string side);
void wageredMillimeter(int &bet, int &drillDistance);
void playRound(int &chosenCard, string &result, string side, vector<string> emperorDeck, vector<string> slaveDeck);
void payout(int &perica, int bet, int &drillDistance, string result, string side);
void ending(int perica, int drillDistance, int round);

int main()
{
	// Variables
	int perica = 0;
	int drillDistance = 30;
	int round = 1;
	int bet;

	// Starting side
	string side = "Emperor";

	// Game Output
	while (round < 13 && drillDistance != 0)
	{
		// UI + Bet
		consoleOutput(perica, drillDistance, round, side);
		wageredMillimeter(bet, drillDistance);

		// Declare Emperor Deck
		vector <string> emperorDeck;
		emperorDeck.push_back("Emperor");
		emperorDeck.push_back("Civilian");
		emperorDeck.push_back("Civilian");
		emperorDeck.push_back("Civilian");
		emperorDeck.push_back("Civilian");

		// Declare Slave Deck
		vector <string> slaveDeck;
		slaveDeck.push_back("Slave");
		slaveDeck.push_back("Civilian");
		slaveDeck.push_back("Civilian");
		slaveDeck.push_back("Civilian");
		slaveDeck.push_back("Civilian");

		system("cls");

		// Variables
		int chosenCard;
		string result = "Draw";

		// Call Functions to start playRound and payout
		playRound(chosenCard, result, side, emperorDeck, slaveDeck);
		payout(perica, bet, drillDistance, result, side);

		// If-statements to switch sides every 3 rounds
		if (round == 3)
			side = "Slave";
		if (round == 6)
			side = "Emperor";
		if (round == 9)
			side = "Slave";

		// Increments round by 1 every iteration
		round++;

		// Reverts the bet back to 0 to restart loop
		bet = 0;
		system("pause");
		system("cls");
	}

	// Call function to evaluate ending
	ending(perica, drillDistance, round);
	system("pause");
	return 0;
}

// Function Definitions
void consoleOutput(int perica, int drillDistance, int round, string side)
{
	// Design
	cout << "                            ============================================================" << endl;
	cout << "                                               E M P E R O R  C A R D" << endl;
	cout << "                            ============================================================" << endl << endl;
	cout << " ==================================" << endl;
	cout << "         ROUND " << round << " OUT OF 12" << endl;
	cout << " ==================================" << endl << endl;

	cout << " Current Perica: " << perica << endl;
	cout << " Millimeters Remaining (mm): " << drillDistance << endl;
	cout << " Current Side: " << side << endl << endl;
}
void wageredMillimeter(int &bet, int &drillDistance)
{
	// User inputs desired bet
	while (bet <= 0 || bet > drillDistance)
	{
		cout << endl;
		cout << " Please enter the amount of millimeters to be wagered: ";
		cin >> bet;
	}
}
void playRound(int &chosenCard, string &result, string side, vector<string> emperorDeck, vector<string> slaveDeck)
{
	// RNG
	srand(time(0));

	// Variables
	int cardsOnHand = 5;

	while (result == "Draw")
	{
		cout << " ==================================" << endl;
		cout << "             C A R D S" << endl;
		cout << " ==================================" << endl << endl;

		// Prints the Emperor Deck
		if (side == "Emperor")
		{
			for (int counter = 0; counter < cardsOnHand; counter++)
				cout << " [" << counter << "]" << emperorDeck[counter] << endl;
		}

		// Prints the Slave Deck
		if (side == "Slave")
		{
			for (int counter = 0; counter < cardsOnHand; counter++)
				cout << " [" << counter << "]" << slaveDeck[counter] << endl;
		}

		// User inputs desired card
		cout << endl;
		cout << " Choose your desired card: ";
		cin >> chosenCard;

		cout << endl << endl;
		cout << " - - - - - - - - - - - - - - - - - - - - - -" << endl;
		cout << "    R E V E A L  Y O U R  C A R D S ! !" << endl;
		cout << " - - - - - - - - - - - - - - - - - - - - - -" << endl << endl;
		system("pause");

		int enemyCard = rand() % cardsOnHand;
		system("cls");

		switch (chosenCard)
		{
			// User chooses first card
		case 0:
		{
			// Player on Emperor Side
			if (side == "Emperor")
			{
				// Emperor Card VS Slave Card
				if (slaveDeck[enemyCard] == "Slave")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You lose!" << endl << endl;
					result = "Lose";
					break;
				}
				// Emperor Card VS Civilian Card
				else if (slaveDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You win!" << endl << endl;
					result = "Win";
					break;
				}
			}
			// Player on Slave Side
			else if (side == "Slave")
			{
				// Slave Card VS Emperor Card
				if (emperorDeck[enemyCard] == "Emperor")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You win!" << endl << endl;
					result = "Win";
					break;
				}
				// Slave Card VS Civilian Card
				else if (emperorDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You lose!" << endl << endl;
					result = "Lose";
					break;
				}
			}
		}
		// User chooses second card
		case 1:
		{
			// Player on Emperor Side
			if (side == "Emperor")
			{
				// Civilian Card VS Slave Card
				if (slaveDeck[enemyCard] == "Slave")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You win!" << endl << endl;
					result = "Win";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (slaveDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					emperorDeck.erase(emperorDeck.begin() + chosenCard);
					slaveDeck.erase(slaveDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
			// Player on Slave Side
			else if (side == "Slave")
			{
				// Civilian Card VS Emperor Card
				if (emperorDeck[enemyCard] == "Emperor")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You lose!" << endl << endl;
					result = "Lose";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (emperorDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					slaveDeck.erase(slaveDeck.begin() + chosenCard);
					emperorDeck.erase(emperorDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
		}
		// User chooses third card
		case 2:
		{
			// Player on Emperor Side
			if (side == "Emperor")
			{
				// Civilian Card VS Slave Card
				if (slaveDeck[enemyCard] == "Slave")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You win!" << endl << endl;
					result = "Win";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (slaveDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					emperorDeck.erase(emperorDeck.begin() + chosenCard);
					slaveDeck.erase(slaveDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
			// Player on Slave Side
			else if (side == "Slave")
			{
				// Civilian Card VS Emperor Card
				if (emperorDeck[enemyCard] == "Emperor")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You lose!" << endl << endl;
					result = "Lose";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (emperorDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					slaveDeck.erase(slaveDeck.begin() + chosenCard);
					emperorDeck.erase(emperorDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
		}
		// User chooses fourth card
		case 3:
		{
			// Player on Emperor Side
			if (side == "Emperor")
			{
				// Civilian Card VS Slave Card
				if (slaveDeck[enemyCard] == "Slave")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You win!" << endl << endl;
					result = "Win";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (slaveDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					emperorDeck.erase(emperorDeck.begin() + chosenCard);
					slaveDeck.erase(slaveDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
			// Player on Slave Side
			else if (side == "Slave")
			{
				// Civilian Card VS Emperor Card
				if (emperorDeck[enemyCard] == "Emperor")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You lose!" << endl << endl;
					result = "Lose";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (emperorDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					slaveDeck.erase(slaveDeck.begin() + chosenCard);
					emperorDeck.erase(emperorDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
		}
		// User chooses fifth card
		case 4:
		{
			// Player on Emperor Side
			if (side == "Emperor")
			{
				// Civilian Card VS Slave Card
				if (slaveDeck[enemyCard] == "Slave")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You win!" << endl << endl;
					result = "Win";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (slaveDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << emperorDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << slaveDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					emperorDeck.erase(emperorDeck.begin() + chosenCard);
					slaveDeck.erase(slaveDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
			// Player on Slave Side
			else if (side == "Slave")
			{
				// Civilian Card VS Emperor Card
				if (emperorDeck[enemyCard] == "Emperor")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "You lose!" << endl << endl;
					result = "Lose";
					break;
				}
				// Civilian Card VS Civilian Card
				else if (emperorDeck[enemyCard] == "Civilian")
				{
					cout << "Your chosen card: " << slaveDeck[chosenCard] << endl << endl;
					cout << "Tonegawa has chosen..." << endl << endl;
					system("pause");
					cout << endl;
					cout << emperorDeck[enemyCard] << endl << endl;
					system("pause");
					cout << endl;
					cout << "It's a draw!" << endl << endl;
					result = "Draw";
					slaveDeck.erase(slaveDeck.begin() + chosenCard);
					emperorDeck.erase(emperorDeck.begin() + enemyCard);
					cardsOnHand -= 1;
					break;
				}
			}
		}
		// User makes bad input
		default:
			break;
		}

		system("pause");
		system("cls");
	}
	// Clears the elements inside emperorDeck and slaveDeck
	emperorDeck.clear();
	slaveDeck.clear();
}
void payout(int &perica, int bet, int &drillDistance, string result, string side)
{
	// Variable
	int reward = 0;

	// Rewards the user if he/she wins
	if (result == "Win")
	{
		if (side == "Emperor")
			reward = bet * 100000;
		else if (side == "Slave")
			reward = bet * 500000;
		cout << "You have obtained " << reward << " Perica!" << endl;
		perica += reward;
	}
	// Loses millimeters if the user has lost
	else if (result == "Lose")
	{
		cout << "The needle has moved by " << bet << "mm." << endl;
		drillDistance -= bet;
	}
}
void ending(int perica, int drillDistance, int round)
{
	if (round > 12 && drillDistance > 0)
	{
		// Best Ending
		if (perica >= 20000000)
		{
			cout << " ==================================" << endl;
			cout << "        B E S T  E N D I N G" << endl;
			cout << " ==================================" << endl << endl;
			cout << " You have reached 20,000,000 Perica!" << endl;
			cout << " Oh, and you still have your ear! Well done!" << endl;
		}
		// Meh Ending
		else if (perica < 20000000)
		{
			cout << " ==================================" << endl;
			cout << "         M E H  E N D I N G" << endl;
			cout << " ==================================" << endl << endl;
			cout << " You didn't reach 20,000,000 Perica :(" << endl;
			cout << " But hey, at least you still have your ear, right?" << endl;
		}
	}
	// Worst Ending
	if (drillDistance <= 0)
	{
		cout << " ==================================" << endl;
		cout << "       W O R S T  E N D I N G" << endl;
		cout << " ==================================" << endl << endl;
		cout << " Maybe you reached 20,000,000 Perica, maybe you didn't." << endl;
		cout << " Point is, you still lost an ear. That doesn't really make you a winner." << endl;
	}
}