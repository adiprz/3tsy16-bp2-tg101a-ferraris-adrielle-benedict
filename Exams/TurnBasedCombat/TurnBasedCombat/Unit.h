#pragma once
#include <string>
#include "Stats.h"
#include <cstdlib>
#include <iostream>

using namespace std;

const float CLASS_ADVANTAGE_DAMAGE_MULTIPLIER = 1.5f;
const int HIT_RATE_MIN = 20;
const int HIT_RATE_MAX = 80;

enum ClassType
{
	ClassWarrior = 0, 
	ClassAssassin = 1, 
	ClassMage = 2
};

class Unit
{
public:
	Unit(string name, ClassType type, Stats stats);
	~Unit();

	// Getters
	string getName();
	Stats* getStats();
	string getClassDescription();
	int getCurrentHp();
	ClassType getClassType();

	// Attacks a target, dealing damage. Target cannot be self.
	void attack(Unit* target);
	// Receive damage. Damage cannot be lower than 1.
	void takeDamage(int damage);
	// Heal unit's HP. Amount cannot be lower than 1.
	void heal(int amount);
	// Check if unit still has HP
	bool alive();
	// Print the stats of the unit
	void displayStatus();

private:

	string mName;
	Stats* mStats;
	int mCurrentHp;
	ClassType mClassType;

	// Check if an attack will hit or not
	bool willHit(Unit* target);
	// Check if this unit's class has advantage over its target. Returns the damage multiplier. Returns 1.0f if there's no bonus damage.
	float evaluateDamageMultiplier(Unit* target);
};

