#pragma once
#include <iostream>

using namespace std;

struct Stats
{
	int hp = 0;
	int power = 0;
	int vitality = 0;
	int agility = 0;
	int dexterity = 0;
};

void addStats(Stats* source, Stats toAdd);
void printStats(Stats stats);