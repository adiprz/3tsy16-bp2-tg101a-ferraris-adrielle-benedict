#pragma once
#include "Unit.h"
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

const int STARTING_STAT_RANGE_MIN = 5;
const int STARTING_STAT_RANGE_MAX = 10;
const int STARTING_HP_MULTIPLIER = 3;
const float STAT_PER_LEVEL = 1.5f;
const float HP_PER_LEVEL = 2;

class UnitSpawner
{
public:
	static Unit* createPlayer();
	static Unit* createOpponent(int level);
	
};

